/*

- Copy your game project code into this file
- for the p5.Sound library look here https://p5js.org/reference/#/libraries/p5.sound
- for finding cool sounds perhaps look here
https://freesound.org/


*/
var gameChar_x;
var gameChar_y;
var floorPos_y;
var scrollPos;
var gameChar_world_x;

var isLeft;
var isRight;
var isFalling;
var isPlummeting;

var game_score;
var flagpole;
var lives;

var jumpSound;
var life_lost;
var run;

function preload()
{
    soundFormats('mp3','wav');
    
    //load your sounds here
    //jumping sound
    jumpSound = loadSound('assets/jump.wav');
    jumpSound.setVolume(0.1);
    
    //when the character loses a life
    life_lost = loadSound('assets/lose_life.wav');
    life_lost.setVolume(0.1);
    
    //when the character is walking
    run = loadSound('assets/background.wav');
    run.setVolume(0.1);
}


function setup()
{
	createCanvas(1024, 576);
    floorPos_y = height * 3/4;
    lives = 4;
    run.play();
    run.loop();
    startGame();
    //background music is played
    //looping the sound clip for the entire duration of the game
}

function draw()
{
	background(100, 155, 255); // fill the sky blue

	noStroke();
	fill(0,155,0);
	rect(0, floorPos_y, width, height/4); // draw some green ground
    
    //scroll
	push();
    translate(scrollPos,0);
    
    // Draw clouds.
    drawClouds();

	// Draw mountains.
    drawMountains();
    
	// Draw trees.
    drawTrees();
    
	// Draw canyons.
    for(var i=0;i<canyon.length;i++)
    {
        drawCanyon(canyon[i]);
        checkCanyon(canyon[i]);
    }

	// Draw collectable items.
    for(var i =0;i < collectable.length;i++)
    {
        if(!collectable[i].isFound)
        {
            drawCollectable(collectable[i]);
            checkCollectable(collectable[i]);
        }
    }
    
    //draw a flagpole
    renderFlagpole();
    
    pop();
    
    //to check if the character has fallen and to re-start the game
    if(isPlummeting == true && lives > 0)
    {
        startGame();
    }
    
    //to display the tokens
    for(var i=0; i < lives; i++)
    {
        fill(255,160,122);
        ellipse((35*[i])+100, 20, 30,30);
    }
    
    //draw screen text
    fill(255)
    noStroke();
    text("SCORE: " + game_score,20,20);
    
    // To check the number of lives and then ending the game
    if(lives < 1)
    {
        fill(0);
        textSize(20);
        text("Game Over!! Press Space to continue", width/2 - 100, floorPos_y/2 - 40);
        return;
    }
    
    // Display Level Complete
    if(flagpole.isReached)
    {
        fill(0);
        textSize(20);
        text("Level Complete!! Press Space to continue", width/2 - 100, floorPos_y/2-60);
        return;
    }
    
    // Draw game character.
	drawGameChar();
    
    // Logic to make the game character move or the background scroll.
	if(isLeft)
	{
		if(gameChar_x > width * 0.2)
		{
			gameChar_x -= 5;
		}
		else
		{
			scrollPos += 5;
		}
	}

	if(isRight)
	{
		if(gameChar_x < width * 0.8)
		{
			gameChar_x  += 5;
		}
		else
		{
			scrollPos -= 5; // negative for moving against the background
		}
	}

	// Logic to make the game character rise and fall.
    if(gameChar_y != floorPos_y)
    {
        gameChar_y+=2;
        isFalling = true;
    }
    else
    {
        isFalling = false;
    }
    if(flagpole.isReached != true)
    {
        checkFlagpole();
    }

	// Update real position of gameChar for collision detection.
	gameChar_world_x = gameChar_x - scrollPos;
}


// ---------------------
// Key control functions
// ---------------------

function keyPressed()
{
    
    if(flagpole.isReached && key == ' ')
    {
        nextLevel();
        return;
    }
    else if(lives == 0 && key == ' ')
    {
        returnToStart();
        return;
    }

	console.log("press" + keyCode);
	console.log("press" + key);
    
    //move the character left
    if(keyCode == 37)
    {
        isLeft = true;
        console.log("isLeft: " + isLeft);
    }
    //move the character right
    if(keyCode == 39)
    {
        isRight = true;
        console.log("isRight: " + isRight); 
    }
    //make the character jump
    if(keyCode == 32 && gameChar_y > 400)
    {
        jumpSound.play(); //when the character jumps, play the sound
        gameChar_y -=100;
    }
}

function keyReleased()
{

	console.log("release" + keyCode);
	console.log("release" + key);
    //stop moving when the left key is released
    if(keyCode == 37)
    {
        isLeft = false;
        console.log("isLeft: " + isLeft);
    }
    //stop moving when the right key is released
    if(keyCode == 39)
    {
        isRight = false;
        console.log("isRight: " + isRight); 
    }
}


// ------------------------------
// Game character render function
// ------------------------------

// Function to draw the game character.

function drawGameChar()
{
    if(isLeft && isFalling)
	{
		// add your jumping-left code
        fill(0);
        rect(gameChar_x, gameChar_y - 6,15,8);
        fill(255,0,0);
        rect(gameChar_x - 10, gameChar_y - 37, 20,35);
        fill(0);
        rect(gameChar_x - 15, gameChar_y - 13,15,8);
        fill(255,160,122);
        ellipse(gameChar_x, gameChar_y - 40, 30,30);
        rect(gameChar_x - 19, gameChar_y - 25,9,8);
    }
	else if(isRight && isFalling)
	{
		// add your jumping-right code
        fill(0);
        rect(gameChar_x - 12, gameChar_y - 6,12,8);
        fill(255,0,0);
        rect(gameChar_x - 10, gameChar_y - 37, 20,35)
        fill(0);
        rect(gameChar_x + 3, gameChar_y - 13,12,8);
        fill(255,160,122);
        ellipse(gameChar_x, gameChar_y - 40, 30,30);
        rect(gameChar_x + 10, gameChar_y - 25,9,8);
	}
	else if(isLeft)
	{
		// add your walking left code
        fill(0);
        rect(gameChar_x , gameChar_y - 6,15,8);
        fill(255,0,0);
        rect(gameChar_x - 5, gameChar_y - 37, 15,35);
        fill(0);
        rect(gameChar_x - 12, gameChar_y - 6,13,8);
        fill(255,160,122);
        ellipse(gameChar_x, gameChar_y - 40, 30,30);
	}
	else if(isRight)
	{
		// add your walking right code
        fill(0);
        rect(gameChar_x - 12, gameChar_y - 6,13,8);
        fill(255,0,0);
        rect(gameChar_x - 10, gameChar_y - 37, 15,35);
        fill(0);
        rect(gameChar_x + 1, gameChar_y - 6,13,8);
        fill(255,160,122);
        ellipse(gameChar_x, gameChar_y - 40, 30,30);


	}
	else if(isFalling)
	{
		// add your jumping facing forwards code
        fill(255,0,0);
        rect(gameChar_x - 10, gameChar_y - 37, 20,35);
        fill(0);
        rect(gameChar_x - 11, gameChar_y - 15,8,8);
        rect(gameChar_x + 3, gameChar_y - 15,8,8);
        fill(255,160,122);
        ellipse(gameChar_x, gameChar_y - 40, 30,30);

	}
	else
	{
		// add your standing front facing code
        fill(255,0,0);
        rect(gameChar_x - 10, gameChar_y - 37, 20,35);
        fill(0);
        rect(gameChar_x - 11, gameChar_y - 8,8,8);
        rect(gameChar_x + 3, gameChar_y - 8,8,8);
        fill(255,160,122);
        ellipse(gameChar_x, gameChar_y - 40, 30,30);

	}
}

// ---------------------------
// Background render functions
// ---------------------------

// Function to draw cloud objects.
function drawClouds()
{
    for(var i=0;i<clouds.length;i++)
    {
        fill(255);
        ellipse(clouds[i].x_pos + 100,clouds[i].y_pos, clouds[i].width, 100);
        ellipse(clouds[i].x_pos + 50,clouds[i].y_pos, clouds[i].width - 20,80);
        ellipse(clouds[i].x_pos + 150,clouds[i].y_pos, clouds[i].width - 20,80);
    }
}

// Function to draw mountains objects.
function drawMountains()
{
    for(var i=0;i<mountains.length;i++)
    {
        fill(205,133,63);
        triangle(mountains[i].x_pos + 280,432, mountains[i].x_pos + 430,mountains[i].height, mountains[i].x_pos + 580,432);
        fill(255);
        triangle(mountains[i].x_pos + 400,250,mountains[i].x_pos + 430,mountains[i].height,mountains[i].x_pos + 460,250)
        fill(205,133,63);
        triangle(mountains[i].x_pos + 525,432, mountains[i].x_pos + 645,mountains[i].height + 74, mountains[i].x_pos + 750,432);
        fill(255);
        triangle(mountains[i].x_pos + 625,306,mountains[i].x_pos + 645,mountains[i].height + 74,mountains[i].x_pos + 663,306)
    }
}

// Function to draw trees objects.
function drawTrees()
{
    for(var i=0;i<trees_x.length;i++)
    {
        fill(139,69,19);
        rect(trees_x[i],370,15,62);
        fill(107,142,35);
        ellipse(trees_x[i] + 7,330,125,125);
        ellipse(trees_x[i] + 7,250,75,75);
    }
}


// ---------------------------------
// Canyon render and check functions
// ---------------------------------

// Function to draw canyon objects.

function drawCanyon(t_canyon)
{
    fill(153,76,0);
    rect(t_canyon.x_pos,432, t_canyon.width + 50,144);
    fill(70,130,180);//water
    rect(t_canyon.x_pos + 55,432, t_canyon.width - 65,144);   
}

// Function to check character is over a canyon.

function checkCanyon(t_canyon)
{
    //canyon jumping
    if(gameChar_world_x > t_canyon.x_pos && gameChar_world_x < t_canyon.width + 50 + t_canyon.x_pos && gameChar_y >= floorPos_y)
    {
        life_lost.play();
        isPlummeting = true;
        if(isPlummeting)
        {
            gameChar_y += 1;
        }
    }
}

// ----------------------------------
// Collectable items render and check functions
// ----------------------------------

// Function to draw collectable objects.

function drawCollectable(t_collectable)
{
    fill(255,255,0);//outer circle
    ellipse(t_collectable.x_pos,t_collectable.y_pos,t_collectable.size - 25,t_collectable.size - 25);
    fill(218,165,32);//inner circle
    ellipse(t_collectable.x_pos,t_collectable.y_pos,t_collectable.size - 40,t_collectable.size - 40);
    fill(75,0,130);//purple gem
    ellipse(t_collectable.x_pos,t_collectable.y_pos - 15,t_collectable.size - 40,t_collectable.size - 40);
    
    
}

// Function to check character has collected an item.

function checkCollectable(t_collectable)
{
    var d = dist(gameChar_world_x,gameChar_y,t_collectable.x_pos,t_collectable.y_pos);
    if (d<20)
    {
        t_collectable.isFound = true;
        game_score += 10;
    }
}

//Function to draw a Flagpole

function renderFlagpole()
{
    push();
    stroke(0);
    strokeWeight(5);
    line(flagpole.x_pos, floorPos_y, flagpole.x_pos, floorPos_y - 200);
    if(flagpole.isReached)
    {
        noStroke();
        fill(224,255,255);
        rect(flagpole.x_pos, floorPos_y - 200, 50, 50);
    }
    else
    {
        noStroke();
        fill(224,255,255);
        rect(flagpole.x_pos, floorPos_y - 75, 50, 50);
    }
    pop();
}

// Function to check character is in range of the flagpole
function checkFlagpole()
{
    var d = abs(gameChar_world_x - flagpole.x_pos);
    if(d<50)
    {
        flagpole.isReached = true;
    }
}

function startGame()
{
    gameChar_x = width/2;
	gameChar_y = floorPos_y;

	// Variable to control the background scrolling.
	scrollPos = 0;

	// Variable to store the real position of the gameChar in the game
	// world. Needed for collision detection.
	gameChar_world_x = gameChar_x - scrollPos;

	// Boolean variables to control the movement of the game character.
	isLeft = false;
	isRight = false;
	isFalling = false;
	isPlummeting = false;

	// Initialise arrays of scenery objects.
    trees_x = [50,500,900,1450,1900];
    clouds = [{x_pos:100,width:100,y_pos: 100},
              {x_pos:1000,width:100,y_pos: 100}];
    mountains = [{x_pos: 0,height: 206},
                {x_pos: 900,height: 206}];
    canyon = [{x_pos: 100, width: 100},
             {x_pos: 1000, width: 100},
             {x_pos: 1600, width: 100}];
    collectable = [{x_pos: 400, y_pos: floorPos_y , size: 55,isFound: false},
                  {x_pos: 950, y_pos: floorPos_y, size: 50,isFound: false},
                  {x_pos: 1500, y_pos: floorPos_y, size: 55,isFound: false},
                  {x_pos: 1800, y_pos: floorPos_y, size: 50,isFound:false}];
    game_score = 0;
    flagpole = {x_pos: 2200, isReached: false};
    lives -= 1;
}